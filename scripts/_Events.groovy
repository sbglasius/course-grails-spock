/**
 * This will add the functional test phase to the project
 */
eventAllTestsStart = {
    if (getBinding().variables.containsKey("functionalTests")) {
        functionalTests << "functional"
    }

}