package demo

import grails.converters.JSON
import grails.converters.XML

import javax.servlet.http.HttpServletResponse

/**
 * See UrlMappings
 */
class DataRestController {
    def dataService

    static allowedMethods = [counties: 'GET', countiesExtended: 'GET']

    static defaultAction = 'counties'

    def counties(boolean enhanced) {
        List<County> counties = enhanced ? dataService.countiesWithMunicipals : dataService.counties
        withFormat {
            html {
                response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "HTML Not supported")
            }
            json {
                render counties as JSON
            }
            xml {
                render counties as XML
            }
        }
    }
}
