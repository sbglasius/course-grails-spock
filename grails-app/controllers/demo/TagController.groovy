package demo
import grails.rest.RestfulController
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TagController extends RestfulController<Tag> {
    TagController() {
        super(Tag)
    }


    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        def resources = listAllResources(params)

        respond resources
    }
}
