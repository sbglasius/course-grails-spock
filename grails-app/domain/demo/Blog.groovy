package demo

class Blog {
    String name
    String content

    SortedSet<BlogEntry> blogEntries

    static hasMany = [blogEntries: BlogEntry]
    static constraints = {
    }
}
