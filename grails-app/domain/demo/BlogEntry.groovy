package demo

class BlogEntry implements Comparable<BlogEntry> {
    String title
    String content
    BlogEntryStatus blogEntryStatus = BlogEntryStatus.DRAFT
    Date publishDate
    Date published
    Date dateCreated // Automatic maintained by Grails

    static belongsTo = [blog: Blog]

    static constraints = {
        publishDate nullable: true, validator: { value, object ->
            if (object.blogEntryStatus == BlogEntryStatus.READY_TO_PUBLISH) {
                if (!value) {
                    return ['must.be.set']
                }
                if(value.before(new Date())) {
                    return ['must.be.in.future']
                }
            }
        }
        publishDate nullable: true
        published nullable: true

    }

    def beforeInsert() {
        updatePublishedDate()
    }

    def beforeUpdate() {
        updatePublishedDate()
    }

    private void updatePublishedDate() {
        if (!published && blogEntryStatus == BlogEntryStatus.PUBLISHED) {
            published = new Date()
        }
    }

    @Override
    int compareTo(BlogEntry other) {
        return publishDate <=> other.publishDate ?: dateCreated <=> other.dateCreated
    }
}
