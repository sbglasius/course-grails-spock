package demo
import grails.transaction.Transactional
import groovy.transform.Memoized

@Transactional
class DataService {
    def grailsApplication
    def restBuilder

    @Memoized
    List<County> getCounties() {
        println "Fetching counties"

        def url = grailsApplication.config.demo.counties

        def resp = restBuilder.get(url)

        resp.json.collect {
            new County(number: it.nr as Integer, name: it.navn)
        }
    }

    @Memoized
    List<Municipal> getMunicipals(County county) {
        println "Fetching municipals for $county"
        def url = "${grailsApplication.config.demo.municipals}?regionsnr=${county.number}"

        def resp = restBuilder.get(url)

        resp.json.collect {
            new Municipal(number: it.nr as Integer, name: it.navn, area: it.areal as Integer)
        }
    }

    County getCounty(String countyName) {
        counties.find { it.name == countyName }
    }

    County getCountyWithMunicipals(String countyName) {
        def county = getCounty(countyName)
        if (county) {
            County countyClone = county.clone()
            countyClone.municipals = getMunicipals(county)
            return countyClone
        }
    }

    List<County> getCountiesWithMunicipals() {
        counties.collect {
            getCountyWithMunicipals(it.name)
        }
    }
}


