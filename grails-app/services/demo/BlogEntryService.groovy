package demo

import grails.transaction.Transactional

@Transactional
class BlogEntryService {

    @Transactional(readOnly = true)
    List<BlogEntry> list(Map params = [:]) {
        BlogEntry.list(params)
    }

    @Transactional(readOnly = true)
    Long count() {
        BlogEntry.count()
    }

    BlogEntry save(BlogEntry blogEntry) {
        blogEntry.save(failOnError: true, flush: true)
    }

    void delete(BlogEntry blogEntry) {
        blogEntry.delete(flush: true)
    }
}
