<%@ page import="demo.Blog" %>



<div class="fieldcontain ${hasErrors(bean: blogInstance, field: 'blogEntries', 'error')} ">
	<label for="blogEntries">
		<g:message code="blog.blogEntries.label" default="Blog Entries" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${blogInstance?.blogEntries?}" var="b">
    <li><g:link controller="blogEntry" action="show" id="${b.id}">${b?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="blogEntry" action="create" params="['blog.id': blogInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'blogEntry.label', default: 'BlogEntry')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: blogInstance, field: 'content', 'error')} required">
	<label for="content">
		<g:message code="blog.content.label" default="Content" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="content" required="" value="${blogInstance?.content}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: blogInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="blog.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${blogInstance?.name}"/>

</div>

