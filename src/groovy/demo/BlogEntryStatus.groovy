package demo

enum BlogEntryStatus {
    DRAFT,
    READY_TO_PUBLISH,
    PUBLISHED,
    ARCHIVED
}