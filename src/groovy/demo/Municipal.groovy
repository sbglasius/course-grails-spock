package demo

import groovy.transform.AutoClone
import groovy.transform.ToString

@ToString(includeFields = true)
@AutoClone
class Municipal {
    int number
    String name
    int area
}
