package demo

import groovy.transform.AutoClone
import groovy.transform.ToString

@ToString(includeFields = true)
@AutoClone
class County {
    int number
    String name

    List<Municipal> municipals
}
