package demo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll
/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(BlogEntry)
class BlogEntryInsertUpdateSpec extends Specification {

    @Unroll
    void "test that only status of PUBLISH sets publishDate when creating blogEntry"() {
        when:
        def blogEntry = new BlogEntry(blogEntryStatus: status).save(validate: false, flush: true) // Will save regardless of validation errors

        then:
        (blogEntry.published != null) == expected

        where:
        status                           | expected
        BlogEntryStatus.DRAFT            | false
        BlogEntryStatus.READY_TO_PUBLISH | false
        BlogEntryStatus.PUBLISHED        | true
        BlogEntryStatus.ARCHIVED         | false
    }

    @Unroll
    void "test that only status of PUBLISH sets publishDate when updating blogEntry"() {
        given:
        def blogEntry = new BlogEntry().save(validate: false, flush: true)

        expect:
        blogEntry.published == null

        when:
        blogEntry.blogEntryStatus = status

        and:
        blogEntry.save(validate: false, flush: true)

        then:
        (blogEntry.published != null) == expected

        where:
        status                           | expected
        BlogEntryStatus.DRAFT            | false
        BlogEntryStatus.READY_TO_PUBLISH | false
        BlogEntryStatus.PUBLISHED        | true
        BlogEntryStatus.ARCHIVED         | false
    }
}
