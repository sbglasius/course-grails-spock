package demo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

@TestFor(BlogEntry)
class BlogEntryValidatorTest extends Specification {

    @Unroll
    def "test validation of publishDate depends on value of blogEntryStatus"() {
        when:
        def blogEntry = new BlogEntry(blogEntryStatus: status, publishDate: date)
        blogEntry.validate(['publishDate'])

        then:
        blogEntry.hasErrors() == expectErrors

        and:
        !expectErrors || blogEntry.errors['publishDate'].code == expectedText

        where:
        status                           | date            | expectErrors | expectedText
        BlogEntryStatus.DRAFT            | null            | false        | null
        BlogEntryStatus.PUBLISHED        | null            | false        | null
        BlogEntryStatus.ARCHIVED         | null            | false        | null
        BlogEntryStatus.READY_TO_PUBLISH | null            | true         | 'must.be.set'
        BlogEntryStatus.READY_TO_PUBLISH | new Date() - 10 | true         | 'must.be.in.future'
        BlogEntryStatus.READY_TO_PUBLISH | new Date() + 10 | false        | null
    }
}
