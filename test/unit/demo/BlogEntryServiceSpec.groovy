package demo

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(BlogEntryService)
@Mock([BlogEntry, Blog])
class BlogEntryServiceSpec extends Specification {
    /**
     * Exercise:
     *
     * 1) Fill in the blanks to make the tests run.
     *    a) Each test has one missing item
     *    b) The entire class has one missing annotation
     */
    def blog

    def setup() {
        blog = new Blog(name: "Blog", content: "Blog content").save(flush: true, failOnError: true)
    }

    void "test that listing blog entries returns the expected entries"() {
        setup: "Create some blog entries"
        createBlogEntries(5)

        when: "You query the service for the first two"
        def list = service.list([max: 2])

        then: "Expect two to be returned and the content of the two to have the right content"
        list.size() == 2
        list[0].title == 'Entry 1'
        list[0].content == 'Entry 1 content'
        list[1].title == 'Entry 2'
        list[1].content == 'Entry 2 content'
    }

    void "test that counting blog entries returns the expected number"() {
        setup: "Create a number of blog entries"
        createBlogEntries(5)

        expect: "Expect that number of blog entries to be returned by our service"
        service.count() == 5
    }

    void "test that a new and validated blog entry can be saved"() {
        setup: "Create blogEntry that can be validated"
        def blogEntry = new BlogEntry(title: "Title", content: "Content", blog: blog)

        expect: "Validate the blogEntry and ensure that it is not saved to the database"
        blogEntry.validate()
        !blogEntry.id

        when: "Save the entry with the service"
        service.save(blogEntry)

        then: "Expect the entry to be saved"
        blogEntry.id
    }

    void "test that an existing blog entry can be saved"() {
        setup: "Create a saved blog entry"
        def blogEntry = new BlogEntry(title: "Title", content: "Content", blog: blog).save(flush: true)

        expect: "Ensure that it is saved"
        blogEntry?.id

        when: "Updating the entry"
        blogEntry.title = "New title"

        and: "Saving it"
        service.save(blogEntry)

        then: "Expect the blog entry version to be one higher than before saving"
        blogEntry.version == old(blogEntry.version)+1
    }

    void "test that a blog entry can be deleted"() {
        setup: "Create some blog entries"
        createBlogEntries(3)
        def entryToDelete = BlogEntry.findByTitle('Entry 2')

        expect: "Make sure that there are actually three elements"
        BlogEntry.count() == 3

        when: "Deleting the entry"
        service.delete(entryToDelete)

        then: "The count of entries is now two"
        BlogEntry.count() == 2

        and: "The first entry is not found in the list"
        !BlogEntry.findByTitle('Entry 2')
    }

    // Helper method to create blogEntries
    List<BlogEntry> createBlogEntries(int count) {
        (1..count).collect {
            new BlogEntry(title: "Entry $it", content: "Entry $it content", blog: blog).save(flush: true)
        }
    }
}
