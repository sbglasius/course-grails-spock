package demo
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.test.mixin.TestFor
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.client.MockRestServiceServer
import spock.lang.Specification

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess
/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(DataService)
class DataServiceSpec extends Specification {
    void "test that no external service is needed when using a GroovySpy"() {
        setup:
        service.restBuilder = Mock(RestBuilder)

        when:
        def result = service.counties

        then:
        1 * service.restBuilder.get(_) >> new RestResponse(new ResponseEntity(responseJson, HttpStatus.OK))

        result.size() == 2

        where:
        responseJson = """[{"nr": "1081","navn": "Region Nordjylland"},{"nr": "1082","navn": "Region Midtjylland"}]"""
    }

    void "test that rest builder is called correct using the restBuilder mock"() {
        setup:
        service.restBuilder = new RestBuilder()

        final mockServer = MockRestServiceServer.createServer(service.restBuilder.restTemplate)
        mockServer.expect(requestTo("http://localhost:8082/kommuner.json?regionsnr=1081"))
                .andExpect(method(HttpMethod.GET))
//                .andExpect(header(HttpHeaders.ACCEPT, "application/json")) // This can be fixed by telling the restBuilder specifically accepting 'application/json'
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON))

        when:
        def result = service.getMunicipals(new County(number: 1081))

        then:
        mockServer.verify()
        result.size() == 4

        where:
        response = '''[{"nr": "0773","navn": "Morsø","areal": "364570000"},{"nr": "0787","navn": "Thisted","areal": "1095180000"},{"nr": "0810","navn": "Brønderslev","areal": "633340000"},{"nr": "0813","navn": "Frederikshavn","areal": "650780000"}]'''


    }
}
