package demo

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.springframework.http.HttpStatus
import spock.lang.Specification
import spock.lang.Unroll

@TestFor(TagController)
@Mock(Tag)
class TagControllerSpec extends Specification {

    @Unroll
    def "test index returns the right content depending on content type requested"() {
        setup:
        new Tag(label: "tag-1").save(flush: true)
        new Tag(label: "tag-2").save(flush: true)

        when:
        request.method = 'GET'
        request.addHeader("Accept", format)

        and:
        controller.index()

        then:
        response.status == HttpStatus.OK.value()
        response.contentAsString == expected

        where:
        format             | expected
        'application/xml'  | '<?xml version="1.0" encoding="UTF-8"?><list><tag id="1"><label>tag-1</label></tag><tag id="2"><label>tag-2</label></tag></list>'
        'application/json' | '[{"class":"demo.Tag","id":1,"label":"tag-1"},{"class":"demo.Tag","id":2,"label":"tag-2"}]'
    }


    @Unroll
    def "test that only POST can be used to create an object"() {
        setup:
        request.method = method
        request.json = [label: 'New Tag']

        when:
        controller.save()

        then:
        response.status == expectedStatus.value()

        where:
        method   | expectedStatus
        'GET'    | HttpStatus.METHOD_NOT_ALLOWED
        'PATCH'  | HttpStatus.METHOD_NOT_ALLOWED
        'PUT'    | HttpStatus.METHOD_NOT_ALLOWED
        'DELETE' | HttpStatus.METHOD_NOT_ALLOWED
        'POST'   | HttpStatus.CREATED
    }

    def "test that it possible to get data as json"() {
        setup:
        def tag = new Tag(label: "My Tag").save(flush: true)

        when:
        params.id = tag.id
        request.addHeader("Accept", 'application/json')

        and:
        controller.show()

        then:
        response.json.label == 'My Tag'
    }

}
