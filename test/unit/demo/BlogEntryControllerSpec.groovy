package demo


import grails.test.mixin.*
import spock.lang.*

@TestFor(BlogEntryController)
@Mock([Blog,BlogEntry])
class BlogEntryControllerSpec extends Specification {

    /**
     * Exercise:
     *
     * 1) Why are the tests failing?
     * 2) How do we fix it?
     */

    def setup() {
        controller.blogEntryService = Mock(BlogEntryService)
    }

    def populateValidParams(params) {
        assert params != null
        params["title"] = 'someValidName'
        params['content'] = 'someOtherValidName'
    }

    void "Test the index action returns the correct model"() {
        setup:
        def blogEntries = createBlogEntries(5)
        when: "The index action is executed"
        controller.index()

        then: "The model is correct"
        1 * controller.blogEntryService.list(_) >> blogEntries
        1 * controller.blogEntryService.count() >> 10
        model.blogEntryInstanceList == blogEntries
        model.blogEntryInstanceCount == 10
    }

    void "Test the create action returns the correct model"() {
        when: "The create action is executed"
        controller.create()

        then: "The model is correctly created"
        model.blogEntryInstance != null
    }

    void "Test the save action correctly persists an instance"() {

        when: "The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def blogEntry = new BlogEntry()
        blogEntry.validate()
        controller.save(blogEntry)

        then: "The create view is rendered again with the correct model"
        model.blogEntryInstance != null
        view == 'create'

        when: "The save action is executed with a valid instance"
        response.reset()
        populateValidParams(params)
        blogEntry = new BlogEntry(params)

        controller.save(blogEntry)

        then: "A redirect is issued to the show action"
        1 * controller.blogEntryService.save(_) >> { BlogEntry entry -> entry.id = 1; entry} // Hacky way of returning a blog entry with id 1 (not really saved)

        response.redirectedUrl == '/blogEntry/show/1'
        controller.flash.message != null
    }

    void "Test that the show action returns the correct model"() {
        when: "The show action is executed with a null domain"
        controller.show(null)

        then: "A 404 error is returned"
        response.status == 404

        when: "A domain instance is passed to the show action"
        populateValidParams(params)
        def blogEntry = new BlogEntry(params)
        controller.show(blogEntry)

        then: "A model is populated containing the domain instance"
        model.blogEntryInstance == blogEntry
    }

    void "Test that the edit action returns the correct model"() {
        when: "The edit action is executed with a null domain"
        controller.edit(null)

        then: "A 404 error is returned"
        response.status == 404

        when: "A domain instance is passed to the edit action"
        populateValidParams(params)
        def blogEntry = new BlogEntry(params)
        controller.edit(blogEntry)

        then: "A model is populated containing the domain instance"
        model.blogEntryInstance == blogEntry
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when: "Update is called for a domain instance that doesn't exist"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        controller.update(null)

        then: "A 404 error is returned"
        response.redirectedUrl == '/blogEntry/index'
        flash.message != null


        when: "An invalid domain instance is passed to the update action"
        response.reset()
        def blogEntry = new BlogEntry()
        blogEntry.validate()
        controller.update(blogEntry)

        then: "The edit view is rendered again with the invalid instance"
        view == 'edit'
        model.blogEntryInstance == blogEntry

        when: "A valid domain instance is passed to the update action"
        response.reset()
        blogEntry = createBlogEntries(1).first()  // Creates a valid blog-entry
        controller.update(blogEntry)

        then: "A redirect is issues to the show action"
        1 * controller.blogEntryService.save(_) >> blogEntry
        response.redirectedUrl == "/blogEntry/show/$blogEntry.id"
        flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when: "The delete action is called for a null instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(null)

        then: "A 404 is returned"
        response.redirectedUrl == '/blogEntry/index'
        flash.message != null

        when: "A domain instance is created"
        response.reset()
        populateValidParams(params)
        def blogEntry = createBlogEntries(1).first() // Creates a valid blog-entry

        then: "It exists"
        BlogEntry.count() == 1

        when: "The domain instance is passed to the delete action"
        controller.delete(blogEntry)

        then: "The instance is deleted"
        1 * controller.blogEntryService.delete(blogEntry)
        // BlogEntry.count() == 0 it makes no sense counting records
        response.redirectedUrl == '/blogEntry/index'
        flash.message != null
    }

    // Helper method to create blogEntries
    List<BlogEntry> createBlogEntries(int count) {
        def blog = new Blog(name: 'Blog',content: 'Content').save(flush: true)
        (1..count).collect {
            new BlogEntry(title: "Entry $it", content: "Entry $it content", blog: blog).save(flush: true)
        }
    }

}
