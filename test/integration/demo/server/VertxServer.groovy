package demo.server

import groovy.json.JsonBuilder
import groovy.util.logging.Log4j
import org.vertx.groovy.core.Vertx
import org.vertx.groovy.core.http.HttpServer
import org.vertx.groovy.core.http.HttpServerRequest

@Log4j
class VertxServer {

    private Vertx vertx = Vertx.newVertx()
    private HttpServer server

    private Map mappings = [:].withDefault { key -> [:] }

    VertxServer(Map params) {
        assert params.host
        assert params.port
        server = vertx.createHttpServer().requestHandler { HttpServerRequest req ->
            def mappingByMethod = mappings[req.method]

            def data = mappingByMethod[req.uri] ?: [statusCode: 404, statusMessage: 'No defined route']
            req.response.with {
                chunked = true
                if (data.statusCode) statusCode = data.statusCode
                if (data.statusMessage) statusMessage = data.statusMessage
                if (data.closure) {
                    def output = data.closure.call()
                    if(!(output instanceof String)) {
                        output = new JsonBuilder(output).toPrettyString()
                    }
                    write(output)
                }
                if (data.json) {
                    def output = new JsonBuilder(data.json).toPrettyString()
                    write(output)
                }
                if (data.text) {
                    write(data.text)
                }
                end()
            }
        }.listen(params.port, params.host)
        log.info "Vertx Server up and ready"
    }

    void registerGet(Map params, String uri) {
        def getMapping = mappings['GET']
        getMapping[uri] = params
    }

    void registerPut(Map params, String uri) {
        def putMapping = mappings['PUT']
        putMapping[uri] = params
    }

    void registerPost(Map params, String uri) {
        def postMappings = mappings['POST']
        postMappings[uri] = params
    }
    void reset() {
        mappings.clear()
    }

    void shutdown() {
        server.close {
            log.info "Shutdown succeeded? " + it.isSucceeded()
        }

    }
}