package demo

import demo.server.VertxServer
import grails.test.spock.IntegrationSpec
import org.springframework.http.HttpStatus
import spock.lang.Shared

class DataServiceIntegrationSpec extends IntegrationSpec {

    def dataService

    @Shared
    VertxServer mockServer = new VertxServer(host: 'localhost', port: 8082)

    def setup() {

    }

    def cleanup() {
        mockServer.shutdown()
    }

    def "when fetching counties the correct list is returned from the remote host"() {
        setup:
        mockServer.registerGet('/regioner.json', statusCode: HttpStatus.OK.value(), json:[[nr: 1081, navn: 'Region Nordjylland'],[nr: 1082, navn: 'Region Midtjylland']] )

        when:
        def result = dataService.counties

        then:
        result.size() == 2
        result[0].number == 1081
        result[0].name == 'Region Nordjylland'
    }

}
