package demo

import grails.test.spock.IntegrationSpec

import javax.servlet.http.HttpServletResponse

class BlogEntryControllerIntegrationSpec extends IntegrationSpec {
    def blogEntryService

    def ctrl = new BlogEntryController()
    Blog blog

    def setup() {
        ctrl.blogEntryService = blogEntryService
        blog = new Blog(name: 'My Blog', content: 'Something').save(failOnError: true, flush: true)
    }

    def "test that an incomplete blogEntry can not be saved "() {
        given:
        ctrl.params.blog = [id: blog.id]
        ctrl.request.method = 'POST'

        when:
        ctrl.save()

        then:
        ctrl.modelAndView.viewName == 'create'
        ctrl.modelAndView.model.blogEntryInstance != null
    }

    def "test that a complete blogEntry can be saved"() {
        given:
        ctrl.params.blog = [id: blog.id]
        ctrl.params.title = 'My Blog Entry'
        ctrl.params.content = 'My Content'
        ctrl.params.blogEntryStatus = 'PUBLISHED'

        ctrl.request.method = 'POST'

        when:
        def model = ctrl.save()

        then:
        def response = ctrl.response
        ctrl.response.status == HttpServletResponse.SC_CREATED
        with(ctrl.modelAndView.model.blogEntryInstance) {
            blog.id == blog.id
            title == 'My Blog Entry'
            content == 'My Content'
            blogEntryStatus == BlogEntryStatus.PUBLISHED
            published != null
            dateCreated != null
        }
    }

}
