package demo

import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import org.springframework.http.HttpStatus
import spock.lang.Specification

class TagFunctionalSpec extends Specification {
    def "test that a tag can be created with call to the Restfull service"() {
        given:
        RestBuilder rest = new RestBuilder()

        when:
        RestResponse response = rest.post("http://localhost:8080/grails-spock/tag/save") {
            // Need to set the accept content-type to JSON, otherwise it defaults to String
            // and the API will throw a 415 'unsupported media type' error
            accept 'application/json'
            json([label: 'Kryf'])
        }

        then:
        response.statusCode == HttpStatus.CREATED
        response.json == [id: 1, class: 'demo.Tag', label: 'Kryf']

    }
}
